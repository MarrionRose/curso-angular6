import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from 'src/app/models/destinos-viajes-state.model';
import { Injectable, forwardRef, Inject, InjectionToken } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class DestinosApiClient {
 destinos:DestinoViaje[];

  constructor(private store: Store<AppState>) {
      this.store
      .select(state => state.destinos)
      .subscribe((data) => {
          console.log('destinos sub store');
          console.log(data);
          this.destinos = data.items;
      });
      this.store
      .subscribe((data) => {
          console.log('all store');
          console.log(data);
      });
    }

  add(d: DestinoViaje) {
    this.store.dispatch(new NuevoDestinoAction(d));
  }

elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

}