import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from 'src/app/models/destinos-api-client.model';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';



class DestinosApiClientViejo {
  getById( id: String ): DestinoViaje {
    console.log('Llamado por la clase vieja!');
    return null;
  }
}


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [ DestinosApiClient,
  { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }
  ]
})
export class DestinoDetalleComponent implements OnInit {
destino:DestinoViaje;

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClientViejo) { }

  ngOnInit(): void {
  const id = this.route.snapshot.paramMap.get('id'); 
  this.destino = null;     
    //this.destino = this.destinosApiClient.getById(id);
  }

}
